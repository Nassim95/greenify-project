# Greenify 🌱
Redévelopper l’outil proposé par l’INR [indicateurs-fragilite](https://indice.institutnr.org/cartographie-indicateurs-fragilite.html) en respectant le plus possible les règles <br>
d’écoconception. Cet outil permet, que vous soyez une commune, un département
ou une région de comparer votre indice de fragilité numérique avec les autres
territoires.
Un utilisateur doit donc pouvoir rechercher l’indice de fragilité d’une commune par
rapport à son département et sa région.


### Setup
docker-compose up -d
cd greenify-frontend
yarn serve

## STACK :
## Vue frontend 🪴
Vue is a client-side JavaScript web framework for developing single page web applications. It works with the MVVM design pattern and supports server side rending since version 2.0.

## Node.js ☘️
Node.js can be used for realizing server-side and well scalable applications. It comes with support for JavaScript / TypeScript and is known for its maximized throughput and efficiency by design.

## MongoDB database 📈
MongoDB is a common representative of NoSQL databases / document stores.
