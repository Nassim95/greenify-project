// const csvtojson = require("csvtojson");
// csvtojson()
//     .fromFile("city_score_delimited.csv")
//     .then(csvData => {
//         console.log(csvData);
//         /*
//         [ { id: '1',
//             name: 'Node.js',
//             description: 'JavaScript runtime environment',
//             createdAt: '2019-09-03' },
//           { id: '2',
//             name: 'Vue.js',
//             description: 'JavaScript Framework for building UI',
//             createdAt: '2019-09-06' },
//           { id: '3',
//             name: 'Angular.js',
//             description: 'Platform for building mobile & desktop web app',
//             createdAt: '2019-09-09' } ]
//         */
//     });

const mongodb = require("mongodb").MongoClient;
const csvtojson = require("csvtojson");
// let url = "mongodb://username:password@localhost:27017/";
let url = "mongodb://root:password@localhost:27017/";
csvtojson()
    .fromFile("city_score.csv")
    .then(csvData => {
        console.log(csvData);
        mongodb.connect(
            url,
            { useNewUrlParser: true, useUnifiedTopology: true },
            (err, client) => {
                if (err) throw err;
                client
                    .db("city_db")
                    .collection("category")
                    .insertMany(csvData, (err, res) => {
                        if (err) throw err;
                        console.log(`Inserted: ${res.insertedCount} rows`);
                        client.close();
                    });
            }
        );
    });