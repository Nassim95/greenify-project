var mongoose  =  require('mongoose');  
   
var csvSchema = new mongoose.Schema({  
    LibCom:{  
        type:String  
    },  
    LibDep:{  
        type:String  
    },  
    LibReg:{  
        type:String  
    },  
    NbPop:{  
        type:Number  
    },  
    DepScore:{  
        type:Number  
    },  
    CityScore:{  
        type:Number  
    },  
    RegScore:{  
        type:Number  
    }
});  
   
module.exports = mongoose.model('cityModel.js',csvSchema);  