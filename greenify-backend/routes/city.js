const { Router } = require("express");
const CityModel = require("../models/mongo/cityModel");
const router = Router();

router.get("", (req, res) => {
  CityModel.find().then((cities) => {
    res.json(cities);
  });
});

router.get("/:id", (req, res) => {
  const id = req.params.id;
  CityModel.findById(id).then((city) => {
    if (city) {
      res.json(city);
    } else {
      res.sendStatus(404);
    }
  });
});

router.get("/:cp", (req, res) => {
  const cp = req.params.cp;
  CityModel.find({ cp: '/'+ cp +'/' }).then((cities) => {
    res.json(cities);
  });
});


router.get("/:name", (req, res) => {
  const name = req.params.name;
  CityModel.find({ name: '/'+ name +'/i' }, 'Libcom Libreg Population' ).then((cities) => {
      res.json(cities);
    
  });
});

module.exports = router;